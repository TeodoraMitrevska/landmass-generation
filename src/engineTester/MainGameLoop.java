package engineTester;
 
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
 
import models.RawModel;
import models.TexturedModel;
import objLoaderImproved.ModelData;
import objLoaderImproved.OBJFileLoader;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;
 
import renderEngine.DisplayManager;
import renderEngine.Loader;
import renderEngine.MasterRenderer;
import renderEngine.OBJLoader;
import terrains.Terrain;
import textures.ModelTexture;
import textures.TerrainTexture;
import textures.TerrainTexturePack;
import entities.Camera;
import entities.Entity;
import entities.Light;
import entities.Player;
 
public class MainGameLoop {
 
    public static void main(String[] args) {
 
        DisplayManager.createDisplay();
        Loader loader = new Loader();
         
        /* OBJECTS LOADED */
        
        ModelData data = OBJFileLoader.loadOBJ("tree");
        RawModel treeModel=loader.loadToVAO(data.getVertices(), data.getTextureCoords(), 
        		data.getNormals(),data.getIndices());
        
        TexturedModel lowPolyTree=new TexturedModel(OBJLoader.loadObjModel("lowpolytree", loader),
        		new ModelTexture(loader.loadTexture("lowpolytree")));
        
       
      
        
        TexturedModel staticModel = new TexturedModel(OBJLoader.loadObjModel("tree", loader),
        		new ModelTexture(loader.loadTexture("tree")));
        
        TexturedModel grass=new TexturedModel(OBJLoader.loadObjModel("grassModel", loader),
        		new ModelTexture(loader.loadTexture("grassTexture")));
        grass.getTexture().setHasTransparency(true);
        grass.getTexture().setUseFakeLighting(true);
        TexturedModel fern=new TexturedModel(OBJLoader.loadObjModel("fern", loader), 
        		new ModelTexture(loader.loadTexture("fern")));
     //   TexturedModel flower=new TexturedModel(OBJLoader.loadObjModel("flower", loader), 
       // 		new ModelTexture(loader.loadTexture("flower")));
        fern.getTexture().setHasTransparency(true);
        fern.getTexture().setHasTransparency(true);
        
        List<Entity> entities = new ArrayList<Entity>();
        Random random = new Random();
        
        
        for(int i=0;i<500;i++){
            //entities.add(new Entity(staticModel, new Vector3f(random.nextFloat()*800 - 400,0,
            		//random.nextFloat() * -600),0,0,0,3));
            entities.add(new Entity(grass, new Vector3f(random.nextFloat()*800 - 400,0,
            		random.nextFloat() * -600),0,0,0,1));
          //  entities.add(new Entity(flower, new Vector3f(random.nextFloat()*800 - 400,0,
            //		random.nextFloat() * -600),0,0,0,1));
            //entities.add(new Entity(fern, new Vector3f(random.nextFloat()*400 - 200,0,
            		//random.nextFloat() * -600),0,0,0,0.6f));
            
            if(i%5==0) {
            	
            	 entities.add(new Entity(lowPolyTree, new Vector3f(random.nextFloat()*600 - 400,0,
                 		random.nextFloat() * -600),0,random.nextFloat()*360,0,random.nextFloat()*0.1f+0.6f));
            	
            }
           
            
         
        }
         
        Light light = new Light(new Vector3f(2000,2000,2000),new Vector3f(1,1,1));
         
       /* MULTITEXTURING*/
        
        TerrainTexture backgroundTexture= new TerrainTexture(loader.loadTexture("grassy"));
        TerrainTexture rTexture=new TerrainTexture(loader.loadTexture("dirt"));
        TerrainTexture gTexture=new TerrainTexture(loader.loadTexture("pinkFlowers"));
        TerrainTexture bTexture=new TerrainTexture(loader.loadTexture("path"));
        
        TerrainTexturePack texturePack=new TerrainTexturePack(backgroundTexture, rTexture,
        		gTexture,bTexture);
        TerrainTexture blendMap=new TerrainTexture(loader.loadTexture("blendMap"));
        
      
		Terrain terrain1 = new Terrain(0,-1,loader,texturePack,blendMap,"heightmap");
        Terrain terrain2 = new Terrain(-1,-1,loader,texturePack,blendMap,"heightmap");
      //  terrain1.getTexturePack().setHasTransparency(true);
        RawModel bunny=OBJLoader.loadObjModel("bunny", loader);
        TexturedModel igralec=new TexturedModel(bunny,new ModelTexture(loader.loadTexture("white")));
        
        Player player=new Player(igralec, new Vector3f(100,0,-50), 0,0,0, 1/2);
        Camera camera = new Camera(player);   
        MasterRenderer renderer = new MasterRenderer(loader);
         
        while(!Display.isCloseRequested()){
            camera.move();
            player.move();
            renderer.processEntity(player);
            renderer.processTerrain(terrain1);
            renderer.processTerrain(terrain2);
            for(Entity entity:entities){
                renderer.processEntity(entity);
            }
            renderer.render(light, camera);
            DisplayManager.updateDisplay();
        }
 
        renderer.cleanUp();
        loader.cleanUp();
        DisplayManager.closeDisplay();
 
    }
 
}